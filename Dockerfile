FROM nginx:alpine

COPY . /usr/share/nginx/html
EXPOSE 1000
CMD ["nginx", "-g", "daemon off;"]